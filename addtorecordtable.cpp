#include "addtorecordtable.h"
#include "ui_addtorecordtable.h"

AddToRecordTable::AddToRecordTable(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddToRecordTable)
{
    ui->setupUi(this);
    connect(ui->edtUserName, SIGNAL(textChanged(QString)),
            this, SLOT(isBadLetter(QString)));

    connect(ui->buttonBox, SIGNAL(accepted()),
                                  this, SLOT(onAccept()));
}

AddToRecordTable::~AddToRecordTable()
{
    delete ui;
}

void AddToRecordTable::SetScores(double scores)
{
    ui->lblScores->setText(QString::number(scores));
}

void AddToRecordTable::isBadLetter(QString bad_text){
    if(bad_text.contains(";")){
        ui->edtUserName->setText(bad_text.replace(";", ""));
    }
}

void AddToRecordTable::setUserNameLabel(QString text){
    UserName = text;
    ui->edtUserName->setText(text);
}

QString AddToRecordTable::getUserName(){
    return UserName;
}

void AddToRecordTable::onAccept(){
    UserName = ui->edtUserName->text();
}

