#-------------------------------------------------
#
# Project created by QtCreator 2015-04-16T22:30:52
#
#-------------------------------------------------

QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Snake
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    addtorecordtable.cpp \
    bestscores.cpp \
    qgamefield.cpp \
    game.cpp \
    gamecontinue.cpp \
    userchooser.cpp \
    player.cpp \
    diffchooser.cpp

HEADERS  += mainwindow.h \
    addtorecordtable.h \
    bestscores.h \
    qgamefield.h \
    game.h \
    gamecontinue.h \
    userchooser.h \
    player.h \
    diffchooser.h

FORMS    += mainwindow.ui \
    addtorecordtable.ui \
    bestscores.ui \
    gamecontinue.ui \
    userchooser.ui \
    diffchooser.ui

RESOURCES += \
    res.qrc

DISTFILES += \
    README.MD

win32:RC_ICONS += snake.ico
