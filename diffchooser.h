#ifndef DIFFCHOOSER_H
#define DIFFCHOOSER_H

#include <QDialog>

namespace Ui {
class DiffChooser;
}

class DiffChooser : public QDialog
{
    Q_OBJECT

public:
    explicit DiffChooser(QWidget *parent = 0);
    ~DiffChooser();

    int getDiff() const;
    void setDiff(int value);

private:
    Ui::DiffChooser *ui;
    int diff;

private slots:
    void setDiff();
};

#endif // DIFFCHOOSER_H
