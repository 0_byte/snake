#ifndef ADDTORECORDTABLE_H
#define ADDTORECORDTABLE_H

#include <QDialog>

namespace Ui {
class AddToRecordTable;
}

class AddToRecordTable : public QDialog
{
    Q_OBJECT

public:
    explicit AddToRecordTable(QWidget *parent = 0);
    ~AddToRecordTable();
    void SetScores(double scores);
    QString UserName;

    void setUserNameLabel(QString text);
    QString getUserName();
signals:
    void getScores();
public slots:
    void onAccept();
private slots:
    void isBadLetter(QString bad_text);

private:
    Ui::AddToRecordTable *ui;
};

#endif // ADDTORECORDTABLE_H
