#ifndef GAME
#define GAME
#include <QApplication>
#include "mainwindow.h"

class Game
{
public:
        static Game& Instance()
        {
                static Game theSingleInstance;
                return theSingleInstance;
        }
        int run(int, char *[]);
private:
        Game(){}
        Game( Game& root);
        Game& operator=(Game&);
};

#endif // GAME

