#include "mainwindow.h"
#include "game.h"

int main(int argc, char *argv[])
 {
     Game &snake = Game::Instance();
     snake.run(argc, argv);

     return 0;
}
