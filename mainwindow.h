#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPicture>
#include <QImage>
#include <QPainter>
#include "player.h"
#include <QtMultimedia/QMediaPlayer>
#include <QtMultimedia/QMediaPlaylist>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
        ~MainWindow();
   QChar k;

   Player player;

   Player getPlayer();
   void setPlayer(Player &value);

   Ui::MainWindow *getUi() const;

   bool getIsNewGame() const;
   void setIsNewGame(bool value);

protected:
   void paintEvent(QPaintEvent *); // перевизначення віртуальної функції

private slots:
    void on_push_new_game();
    void on_game_over();
    void refresh_scores(int val);
    void refresh_speed(int val);


    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_push_continue();

    void gameContinue();
    void gameStart();
    void playSound(int index);
    void soundMuting();

    void supperAppleUnvis();
    void supperAppleVis();
    void rottenAppleUnvis();
    void rottenAppleVis();


private:
    Q_DISABLE_COPY(MainWindow)
    Ui::MainWindow *ui;

    bool isNewGame;    
    QMediaPlayer *aplayer;
    QMediaPlaylist *playlist;

    void setEnabledUI(QString mainButtText, bool enabled);
    void initPlayer();
};

#endif // MAINWINDOW_H
