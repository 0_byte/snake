#ifndef GAMECONTINUE_H
#define GAMECONTINUE_H

#include <QDialog>

namespace Ui {
class GameContinue;
}

class GameContinue : public QDialog
{
    Q_OBJECT
public:
    explicit GameContinue(QWidget *parent = 0 );
    ~GameContinue();

private slots:
    void on_button_continue();
    void on_button_stop();

private:
    Ui::GameContinue * ui;

};

#endif // GAMECONTINUE_H
