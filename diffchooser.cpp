#include "diffchooser.h"
#include "ui_diffchooser.h"

DiffChooser::DiffChooser(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DiffChooser)
{
    ui->setupUi(this);
    connect(ui->okButt, SIGNAL(released()), this, SLOT(setDiff()));
}

DiffChooser::~DiffChooser()
{
    delete ui;
}
int DiffChooser::getDiff() const
{
    return diff;
}

void DiffChooser::setDiff(int value)
{
    diff = value;
}

void DiffChooser::setDiff()
{
    diff = ui->diffSlider->value();
}

