#ifndef QGAMEFIELD_H
#define QGAMEFIELD_H

#include <QFrame>
#include <QPainter>
#include <QObject>
#include <QPixmap>

//Поточний напрям руху змійки
enum direction{ dir_up = 1, dir_down = 3,
             dir_right = 2, dir_left = 4 };

//Клас для ігрового поля
class QGameField : public QFrame
{
    Q_OBJECT

private:
    //Поточний напрям руху
    direction m_dir;
    //Чи запущена гра
    bool is_running;
    //Ширина комірки змійки
    int cell_width;
    //Висота комірки змійки
    int cell_height;
    //Кількість з'їдених яблук
    int scores;
    //Рівень складності: швидкість гри, зростає в міру набору очок
    int speed_rate;
    //Таймер для руху
    QTimer *m_timer;
    QTimer *timer;
    QTimer *appleTimer;
    QTimer *snakeTimer;

    QTimer *rottenTimer;
    QTimer *superTimer;

    QTimer *goodTimer;

    QTimer *drawTimer;

    QPixmap *appleSprite;
    QPixmap *snakeBody;
    QPixmap *snakeHead;
    QPixmap *rottenAppleSprite;
    QPixmap *superAppleSprite;

    bool isAppleScaled;
    int snakeAnim;
    bool isSnakeMove;

    //game's difficulty
    int diff;
    //Координати яблука
    QPoint apple;
    QPoint superApple;
    QPoint rottenApple;

    QList<QPoint> obstacles;

    //Намалювати ігрову сітку
    void draw_fields(QPainter *p);
    //Будує початкову змійку
    void init_snake();
    //Перебудовує змійку при продовжені гри
    void rebuild_snake(int size);
    //Малює змійку на екрані
    void draw_snake(QPainter *p);
    //Малює яблуко
    void draw_apple(QPainter *p);
    void draw_scalled_apple(QPainter *p);
    void drawSupperApple(QPainter *p, QPoint point, QPixmap *sprite, bool scalled);
    void drawObstacles(QPainter *p);
    //Перевіряє чи лежить точка, яка нас цікавить всередині змійки
    bool is_in_snake(QPoint &p);
    //Знаходить нові координати яблуку
    void refresh_apple();
    QPoint initRandomPoint();
    //Сеттер для очків, посилає всередині сигнал OnChangeScores
    void set_scores(int val);
    void set_speed_rate(int rate);
    //Реверс змійки
    void snake_reverse();
    void diffParser();

    void getRottenApple();
    void getSuperApple();
    void initObstacles(int diff);

private slots:
    //Переміщує змійку
    void move_snake();
    void appleAnimation();
    void animSnake();

    void startSupperApple();
    void startRottenApple();
    void stopSupperApple();
    void stopRottenApple();

    void stopGoodTimer();
    void drawRemains();

public:
    //Масив координат змійки
    QList<QPoint> snake;
    //Розмір поля
    const static int rows=50;
    const static int colums=50;
    //Швидкість (інтервал спрацьовування таймера в мілісекундах)
    const static int speed = 250;
    explicit QGameField(QWidget *parent);
    ~QGameField();
    void StartGame(double game_diff);
    void ContinueGame(double score, double old_speed, int diff,int snake_size);
    void StopGame();
    bool IsGameRunning();
//    void StartGamee();

    int getDiff() const;
    void setDiff(int value);

    int getScores() const;

    void setScores(int value);

signals:
    //Закінчення гри
    void OnGameOver();
    //Змінилась кількість з'їдених яблук
    void OnChangeScores(int scores);
    //Змінилась швидкість
    void onChangeSpeedRate(int rate);
    void sendPlaySound(int index);

    void sendStartRottenTimer();
    void sendStartSuperTimer();
    void sendStopSuperTimer();
    void sendStopRottenTimer();

    void sendSuperTime(int time);
    void sendIsSuperAppleActive(bool isactive);

    protected:
    //Перехоплюємо події натискання на клавіатуру та перемалювання
    virtual void paintEvent(QPaintEvent *event);
    virtual void resizeEvent(QResizeEvent *event);
    //Тут будуть перехоплюватися натискання на кнопки на клавиатурі
    virtual bool eventFilter(QObject *object, QEvent *event);
};

#endif // QGAMEFIELD_H
