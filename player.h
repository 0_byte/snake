#ifndef PLAYER_H
#define PLAYER_H

#include <QString>
#include <QObject>

class Player
{

public:
    Player();
    Player(QString name, double score, double diff, int snake_size, QString lastgame);

    QString getName() const;
    void setName(const QString &value);

    double getScore() const;
    void setScore(double value);

    double getDifficulty() const;
    void setDifficulty(double value);

    int getSnake_size() const;
    void setSnake_size(int value);

    QString getLast_game() const;
    void setLast_game(const QString &value);

    double getSpeed_rate() const;
    void setSpeed_rate(double value);

private:
    QString name;
    double score;
    double difficulty;
    int snake_size;
    double speed_rate;

    QString last_game;
};




#endif // PLAYER_H
