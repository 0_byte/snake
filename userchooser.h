#ifndef USERCHOOSER_H
#define USERCHOOSER_H

#include <QDialog>
//#include <QtCore/QMap>
#include <QMap>
#include "player.h"

namespace Ui {
class UserChooser;
}

class UserChooser : public QDialog
{
    Q_OBJECT

public:
    explicit UserChooser(QWidget *parent = 0);
    ~UserChooser();

    Player getP() const;
    void setP(const Player &value);

    bool getIsChoosed() const;
    void setIsChoosed(bool value);

    bool getIsBadData() const;
    void setIsBadData(bool value);

private slots:
    //display current player score
    void displayScore(int x);
    void buttonOkClick();
    void buttonCancelClick();   

signals:
    void gameContinue();
    void choosedPlayer();
private:
    Ui::UserChooser *ui;

    // map with players
//    QMap<QString, QString> players;
    QList<Player> pl;
    Player p;

    //will check if player was choosed
    bool isChoosed;
    bool isBadData;

    //init data from score file
    bool initChooser();
};

#endif // USERCHOOSER_H
