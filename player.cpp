#include <QObject>
#include "player.h"


Player::Player()
{
    name = "TestPlayer";
    difficulty = 1;
}

Player::Player(QString name, double score, double diff, int snake_size, QString lastgame){
    this->name = name;
    this->score = score;
    this->difficulty = diff;
    this->snake_size = snake_size;
    this->last_game = lastgame;
}

QString Player::getName() const
{
    return name;
}

void Player::setName(const QString &value)
{
    name = value;
}
double Player::getScore() const
{
    return score;
}

void Player::setScore(double value)
{
    score = value;
}
double Player::getDifficulty() const
{
    return difficulty;
}

void Player::setDifficulty(double value)
{
    difficulty = value;
}
int Player::getSnake_size() const
{
    return snake_size;
}

void Player::setSnake_size(int value)
{
    snake_size = value;
}
QString Player::getLast_game() const
{
    return last_game;
}

void Player::setLast_game(const QString &value)
{
    last_game = value;
}
double Player::getSpeed_rate() const
{
    return speed_rate;
}

void Player::setSpeed_rate(double value)
{
    speed_rate = value;
}








