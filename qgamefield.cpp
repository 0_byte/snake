#include "qgamefield.h"
#include "mainwindow.h"

#include <QMessageBox>
#include <QKeyEvent>
#include <QtGui>
#include <QtCore/QDebug>
#include <QTimer>


//Малює ігрову сітку

int QGameField::getDiff() const
{
    return diff;
}

void QGameField::setDiff(int value)
{
    diff = value;
}

int QGameField::getScores() const
{
    return scores;
}

void QGameField::setScores(int value)
{
    scores = value;
}
void QGameField::draw_fields(QPainter *p)
{
    // p->fillRect(this->rect(), Qt::White);
    p->setPen(Qt::gray);
    for (int i=0; i<rows; ++i)
    {
        p->drawLine(0, i*cell_height, width(), i*cell_height);
    }
    for (int i=0; i<colums; ++i)
    {
        p->drawLine(i*cell_width, 0, i*cell_width, height());
    }
}

void QGameField::init_snake()
{
    snake.clear();
    //Змійка з 3-х елементів в центрі екрану
    int center_x = rows/2;
    int center_y = colums /2;
    snake.append(QPoint(center_x-1, center_y));
    snake.append(QPoint(center_x, center_y));
    snake.append(QPoint(center_x+1, center_y));
    update();
}

void QGameField::rebuild_snake(int size){
    snake.clear();
    int center_x = rows/2;
    int center_y = colums /2;
    for(int i = 0; i < size; i++){
        snake.append(QPoint(center_x-1, center_y-1));
    }
    update();
}

void QGameField::draw_snake(QPainter *p)
{
    //Малюємо масив квадратиків на поле
    for (int i=0; i<snake.count(); ++i)
    {
        QColor col;
        //Голову змії замальовуємо червоним кольором, інше синім
        QPoint cell = snake[i];
        if(i == snake.count()-1){
            p->drawPixmap(cell.x()*cell_width-1,
                           cell.y()*cell_height-1,
                          12,12,
                          *snakeHead, snakeAnim, 0, 50, 50);
        }else{
            p->drawPixmap(cell.x()*cell_width,
                          cell.y()*cell_height,
                          cell_width,
                          cell_height,
                          *snakeBody);
        }

    }

}

void QGameField::draw_apple(QPainter *p)
{
  p->drawPixmap(apple.x()*cell_width,
                apple.y()*cell_height,
                cell_width,
                cell_height,
                *appleSprite);
}

void QGameField::draw_scalled_apple(QPainter *p){
    p->drawPixmap((apple.x()*cell_width)-1,
                  (apple.y()*cell_height)-1,
                  cell_width+2,
                  cell_height+2,
                  *appleSprite);
}

void QGameField::drawSupperApple(QPainter *p, QPoint point, QPixmap *sprite, bool scalled)
{
    if(scalled){
        p->drawPixmap((point.x()*cell_width-1),
                      (point.y()*cell_height-1),
                      cell_width+2,
                      cell_height+2,
                      *sprite);
    }else{
        p->drawPixmap((point.x()*cell_width),
                      (point.y()*cell_height),
                      cell_width,
                      cell_height,
                      *sprite);
    }

}

void QGameField::drawObstacles(QPainter *p)
{
    for(int i=0; i< obstacles.size()-1; ++i){
        QColor col = Qt::darkGray;
        QRect r = QRect(obstacles[i].x()*cell_width,
                        obstacles[i].y()*cell_height,
                        cell_width,
                        cell_height);
        p->fillRect(r, col);
        p->setPen(Qt::black);
        p->drawRect(r);
    }
}

bool QGameField::is_in_snake(QPoint &p)
{
  for (QList<QPoint>::iterator i=snake.begin(); i!=snake.end(); ++i)
  {
    if (i->x() == p.x() && i->y()==p.y())
        return true;
  }
  return false;
}

void QGameField::refresh_apple()
{
    //Генеруємо випадкову точку до тих пір
    //поки вона не буде поза нашою змійкою       
    do{
        apple.setX(qrand()%rows);
        apple.setY(qrand()%colums);
    }
    while (is_in_snake(apple)
           && apple.x() != superApple.x()
           && apple.y() != superApple.y()
           && apple.y() != rottenApple.y()
           && apple.x() != rottenApple.x());

    for(int i=0; i<obstacles.size()-1; ++i){
        if(obstacles[i].x() == apple.x()
                && obstacles[i].y() == apple.y()){
                refresh_apple();
        }
    }
}

QPoint QGameField::initRandomPoint()
{
    QPoint *p = new QPoint();
    do{
        p->setX(qrand()%rows);
        p->setY(qrand()%colums);
    }
    while (is_in_snake(*p)
           && apple.x() != p->x()
           && apple.y() != p->y()
           && p->x() != superApple.x()
           && p->y() != superApple.y()
           && p->y() != rottenApple.y()
           && p->x() != rottenApple.x());
    return *p;
}

void QGameField::set_scores(int val)
{
    if (scores != val)
    {
        scores = val;

        diffParser();
        emit OnChangeScores(val);
    }
}

void QGameField::set_speed_rate(int rate)
{
    speed_rate = rate;    
    m_timer->setInterval(speed-2*(speed_rate));
    emit onChangeSpeedRate(rate);
}

void QGameField::snake_reverse()
{
    for (int i=0; i<snake.count() /2; ++i)
    {
      snake.swap(i, snake.count()-i-1);
    }
}

void QGameField::move_snake()
{
  if(!is_running) {return;}
  QPoint next_point = snake.last();
  switch(m_dir){
   case dir_down:{
      next_point.setY(next_point.y()+1);
      break;
      }
   case dir_left:{
      next_point.setX(next_point.x()-1);
      break;
  }
   case dir_right:{
      next_point.setX(next_point.x()+1);
      break;
  }
  case dir_up:{
      next_point.setY(next_point.y()-1);
      break;
  }
  }

  //Робимо стіни "прозорі", тобто змійка, виходячи
  //за межі ігрового поля з'являється з іншої сторони
  if (next_point.x() < 0)
      next_point.setX(rows-1);
  if (next_point.x()>=rows)
      next_point.setX(0);
  if (next_point.y() < 0)
      next_point.setY(colums-1);
  if (next_point.y()>=colums )
      next_point.setY(0);

  if(superTimer->isActive() && is_in_snake(superApple)){
      getSuperApple();
      emit sendPlaySound(2);
  }

  if(rottenTimer->isActive() && is_in_snake(rottenApple)){
     getRottenApple();
     emit sendPlaySound(2);
  }

  for(int i=0; i<obstacles.size()-1; ++i){
      if(next_point.x() == obstacles[i].x()
              && next_point.y() == obstacles[i].y()){
          StopGame();
      }
  }

  //Якщо є самоперетин, помирає
  if (is_in_snake(next_point))
  {
      StopGame();
  }
  else
  {

    //Добавляємо елемент в голову
    snake.append(next_point);
    //Якщо потрапили в яблуко не видаляємо хвіст
    if (is_in_snake(apple))
    {
      refresh_apple();
      set_scores(scores + (1*diff));      
      emit sendPlaySound(2);
    }
    else
    {
      //Видаляємо хвіст
      snake.removeFirst();
    }
  }
//  repaint();
  update();
}

void QGameField::appleAnimation(){
    if(!isAppleScaled) isAppleScaled = true;
    else isAppleScaled = false;
}
void QGameField::animSnake(){
    if(isSnakeMove){
        //draw with directions
        if(m_dir == dir_left){
            snakeAnim = 200;
        }else if(m_dir == dir_right){
            snakeAnim = 0;
         }else if(m_dir == dir_up){
            snakeAnim = 100;
         }else if(m_dir == dir_down){
            snakeAnim = 150;
        }
        isSnakeMove = false;
    }else{
        //draw circle
        snakeAnim = 50;
        isSnakeMove = true;
    }
}

QGameField::QGameField(QWidget *parent):
     QFrame(parent)
{
    scores=0;
    speed_rate = 0;
    m_dir = dir_left;
    is_running = false;
    parent->installEventFilter(this);

    m_timer = new QTimer(this);
    connect(m_timer, SIGNAL(timeout()), this, SLOT(move_snake()));

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()),this, SLOT(move_snake()));

    drawTimer = new QTimer(this);
    connect(drawTimer, SIGNAL(timeout()), this, SLOT(drawRemains()));
    drawTimer->setInterval(1000);
    drawTimer->start(1000);

    init_snake();
    refresh_apple();

    isAppleScaled = false;

    snakeBody = new QPixmap(":/snakebody.png");
    appleSprite = new QPixmap(":/apple.png");
    snakeHead = new QPixmap(":/pacnew.png");
    rottenAppleSprite = new QPixmap(":/rottenApple.png");
    superAppleSprite = new QPixmap(":/yellowApple.png");

    appleTimer = new QTimer(this);
    connect(appleTimer, SIGNAL(timeout()), this, SLOT(appleAnimation()));

    superTimer = new QTimer(this);
    rottenTimer = new QTimer(this);
    connect(this, SIGNAL(sendStartSuperTimer()), this, SLOT(startSupperApple()));
    connect(this, SIGNAL(sendStartRottenTimer()), this, SLOT(startRottenApple()));
    connect(this, SIGNAL(sendStopSuperTimer()), this, SLOT(stopSupperApple()));
    connect(this, SIGNAL(sendStopRottenTimer()), this, SLOT(stopRottenApple()));

    connect(superTimer, SIGNAL(timeout()), this, SLOT(stopSupperApple()));
    connect(rottenTimer, SIGNAL(timeout()), this, SLOT(stopRottenApple()));
    connect(superTimer, SIGNAL(timeout()), this, SIGNAL(sendStopSuperTimer()));
    connect(rottenTimer, SIGNAL(timeout()), this, SIGNAL(sendStopRottenTimer()));

    snakeAnim = 0;
    isSnakeMove = false;
    snakeTimer = new QTimer(this);
    connect(snakeTimer, SIGNAL(timeout()), this, SLOT(animSnake()));

    goodTimer = new QTimer(this);
    connect(goodTimer, SIGNAL(timeout()), this, SLOT(stopGoodTimer()));

    emit sendSuperTime(0);
}

QGameField::~QGameField()
{
}

void QGameField::StartGame(double game_diff)
{
  this->diff = game_diff;
  set_scores(0);
  is_running = true;
  init_snake();
  initObstacles(game_diff);
  m_dir = dir_right;

  m_timer->start(50);
  appleTimer->start(250);

  snakeTimer->start(150);

  if(timer->isActive()){
      if(diff >= 5){
          timer->start(0);
      }
  }

  diffParser();

  emit sendStopSuperTimer();
  emit sendStopRottenTimer();

}

void QGameField::ContinueGame(double score, double old_speed, int diff, int snake_size){
    is_running = true;
    this->diff = diff;

    set_scores(score);
    rebuild_snake(snake_size);
    initObstacles(diff);

    m_dir = dir_right;

    if(old_speed > 125){
        m_timer->start(30);
    }else{
        m_timer->start(speed-2*old_speed);
    }

    appleTimer->start(150);
    snakeTimer->start(200);

    if(timer->isActive()){
        if(diff >= 5){
            timer->start(0);
        }
    }

    diffParser();

    emit sendStopSuperTimer();
    emit sendStopRottenTimer();
}

void QGameField::StopGame()
{
  is_running = false;
  m_timer->stop();
  appleTimer->stop();

  if(rottenTimer->isActive()){
      rottenTimer->stop();
  }

  if(superTimer->isActive()){
      superTimer->stop();
  }

  if(timer->isActive()){
    timer->stop();
  }
  emit OnGameOver();
  emit sendStopSuperTimer();
  emit sendStopRottenTimer();

  scores = 0;
  speed_rate = 0;
  diff = 1;
}

bool QGameField::IsGameRunning()
{
    return is_running;
}

void QGameField::paintEvent(QPaintEvent *event)
{
    QPainter painter(this); // визначає об'єкт painter, який забезпечує малювання
    QPainter paint(this);

    draw_fields(&paint);
    if(isAppleScaled){
        draw_apple(&paint);
    }else{
        draw_scalled_apple(&paint);
    }

    if(superTimer->isActive()){
        drawSupperApple(&paint, superApple, superAppleSprite, isAppleScaled);
    }

    if(rottenTimer->isActive()){
        drawSupperApple(&paint, rottenApple, rottenAppleSprite, isAppleScaled);
    }

    draw_snake(&paint);
    drawObstacles(&paint);

    QFrame::paintEvent(event);
}

void QGameField::resizeEvent(QResizeEvent *event)
{
    QFrame::resizeEvent(event);
    cell_width = width() / rows;
    cell_height = height() / colums;
}

bool QGameField::eventFilter(QObject *object, QEvent *event)
{

    if (event->type() == QEvent::KeyPress && is_running)
    {
      QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
      direction new_dir = m_dir;
      //Змінюємо напрям руху змії в залежності від натиснутих клавіш
      switch(keyEvent->key()){
      case Qt::Key_Left:{
          new_dir = dir_left;
      } break;
      case Qt::Key_Right:{
          new_dir = dir_right;
      } break;
      case Qt::Key_Down:{
          new_dir = dir_down;
      } break;
      case Qt::Key_Up:{
          new_dir = dir_up;
      } break;

      default: return false;
     }

      //Робимо реверс змійки у випадку протилежних напрямів
      if (abs(m_dir-new_dir)==2)
        snake_reverse();
      m_dir = new_dir;
      return true;
    }
    else
       return QFrame::eventFilter(object, event);
}

void QGameField::diffParser(){
    int level = snake.size()-3;

    if (speed_rate > 124)
      set_speed_rate(124);
    else{        
        //increase speed every 3 level
            if(level % 3 == 0){
                set_speed_rate(speed_rate + 4);                
            }
    }

    if(level % 5 == 0){
        emit sendStartSuperTimer();
    }

    if(level % 6 == 0){
        emit sendStartRottenTimer();
    }

    switch (diff) {
    case 5:
        timer->setInterval(250);
        emit onChangeSpeedRate(speed_rate);
        break;
    case 6:
        timer->setInterval(200);
        m_timer->setInterval(200);
        speed_rate = 50;
         emit onChangeSpeedRate(150);
        break;
    case 7:
        timer->setInterval(150);
        m_timer->setInterval(150);
        speed_rate = 50;
         emit onChangeSpeedRate(250);
        break;
    case 8:
        timer->setInterval(100);
        m_timer->setInterval(100);
        speed_rate = 75;
         emit onChangeSpeedRate(350);
        break;
    case 9:
        timer->setInterval(50);
        m_timer->setInterval(50);
        speed_rate = 100;
         emit onChangeSpeedRate(400);
        break;
    case 10:
        timer->setInterval(40);
        m_timer->setInterval(30);
        speed_rate = 124;
        emit onChangeSpeedRate(430);
        break;

    default:
        break;

    }
    if(&goodTimer != NULL){
        if(goodTimer->isActive()){
            speed_rate -= 15;
            m_timer->setInterval(speed-2*(speed_rate));
            emit onChangeSpeedRate(500 - m_timer->interval() - timer->interval());
        }
    }
}

void QGameField::getRottenApple()
{
    scores = scores - (2*diff);
    emit OnChangeScores(scores);
    emit sendStopRottenTimer();
}

void QGameField::getSuperApple()
{
    scores = scores + (2*diff);
    emit OnChangeScores(scores);

    goodTimer->start(10000);

    speed_rate -= 15;
    m_timer->setInterval(speed-2*(speed_rate));
    emit onChangeSpeedRate(500 - m_timer->interval() - timer->interval());

    emit sendStopSuperTimer();
}

void QGameField::initObstacles(int diff)
{
    obstacles.clear();
    for(int i=0; i<diff*5; i++){
        obstacles.append(initRandomPoint());
    }
}

void QGameField::startSupperApple()
{
    superApple = initRandomPoint();
    superTimer->start(7000);
}

void QGameField::startRottenApple()
{
    rottenApple = initRandomPoint();
    rottenTimer->start(7000);
}

void QGameField::stopSupperApple()
{
    superTimer->stop();
    emit sendSuperTime(0);

}

void QGameField::stopRottenApple()
{
    rottenTimer->stop();
}

void QGameField::stopGoodTimer()
{
    goodTimer->stop();
    speed_rate += 15;
    m_timer->setInterval(speed-2*(speed_rate));    
    emit onChangeSpeedRate(500 - m_timer->interval() - timer->interval());
    emit sendIsSuperAppleActive(false);
}

void QGameField::drawRemains()
{
    if(superTimer != NULL){
        if(superTimer->isActive()){
            if( superTimer->remainingTime() == -1){
                emit sendSuperTime(0);
                emit sendIsSuperAppleActive(false);
            }else if(superTimer->remainingTime() == 0){
                emit sendSuperTime(0);
                emit sendIsSuperAppleActive(false);
            }else{
                emit sendSuperTime(superTimer->remainingTime()/350);
                emit sendIsSuperAppleActive(true);
            }
        }
    }
}
