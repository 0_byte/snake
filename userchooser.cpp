#include "userchooser.h"
#include "ui_userchooser.h"
#include <QMessageBox>

#include <QFile>
#include <QtCore/QDebug>

UserChooser::UserChooser(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UserChooser)
{
    ui->setupUi(this);    
    isChoosed = false;

    //read all user data from file
    if(initChooser()){
        ui->combo_choose_user->setCurrentIndex(0);
        displayScore(0);
    }else{
        this->close();
    }

    //will connect user chooser and score highlighter
    connect(ui->combo_choose_user,
            SIGNAL(currentIndexChanged(int)),
            this,
            SLOT(displayScore(int)));

    connect(ui->pushOkButton,
            SIGNAL(clicked(bool)),
            this, SLOT(buttonOkClick()));

    connect(ui->pushCancelButton_2,
            SIGNAL(clicked(bool)),
            this, SLOT(buttonCancelClick()));

}

//Load player's data from savefile and add it to dropdownlist
bool UserChooser::initChooser(){
    QFile data;
    data.setFileName(QCoreApplication::applicationDirPath().append("/scores.txt"));

    QString rawdata;
    QStringList rows;
    QStringList cols;
    rawdata.clear();
    rows.clear();
    cols.clear();

    if(data.exists()){
        data.open(QFile::ReadOnly);
        rawdata = data.readAll();
        data.close();

        if(rawdata.isEmpty()){
             QMessageBox::warning(0,"Помилка!", "Не можу зчитати файл збережень");
        }
        rows = rawdata.split("\n");

        for (int i=0; i<rows.count(); ++i){
            //Skip line if starts with // or empty
           if(rows[i].startsWith("//") || rows[i].isEmpty()){
                continue;
              }
             cols = rows[i].split(";");
             if (cols.count() == 6){

                Player p;
                p.setName(cols[0]);
                p.setScore( cols[1].toDouble());
                p.setDifficulty(cols[2].toDouble());
                p.setSnake_size(cols[3].toInt());
                p.setSpeed_rate(cols[4].toDouble());
                p.setLast_game(cols[5]);
                pl.append(p);

                ui->combo_choose_user->addItem(p.getName());                
             }else{
                QMessageBox::warning(0,"Помилка!", "Не можу зчитати файл збережень");
                return false;
             }
        }    
        return true;
    }else{
        QMessageBox::warning(0,"Помилка!", "Файл scores.txt не знайденно");
        QFile f;
        f.setFileName(QCoreApplication::applicationDirPath().append("/scores.txt"));
        f.open(QIODevice::Append| QFile::Text);
        f.close();
        return false;
    }
}

//Display player's scores
void UserChooser::displayScore(int x){
    setP(pl.at(x));
    ui->label_score->setText(QString::number(pl.at(x).getScore()));
    isChoosed = true;
}

void UserChooser::buttonCancelClick(){
    this->close();
}
bool UserChooser::getIsBadData() const
{
    return isBadData;
}

void UserChooser::setIsBadData(bool value)
{
    isBadData = value;
}

bool UserChooser::getIsChoosed() const
{
    return isChoosed;
}

void UserChooser::setIsChoosed(bool value)
{
    isChoosed = value;
}


Player UserChooser::getP() const
{
    return p;
}

void UserChooser::setP(const Player &value)
{
    p = value;
}


void UserChooser::buttonOkClick(){
    if(isChoosed){
        close();
    }else{
        QMessageBox::information(0,"Виберiть гравця", "Виберiть гравця з видаючого меню для продовження");
    }
}


UserChooser::~UserChooser()
{
    delete ui;
}
