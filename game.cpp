#include "mainwindow.h"
#include "game.h"
#include <QApplication>
#include <QLabel>
#include <QSplashScreen>
#include <QTimer>

int Game::run(int argc, char *argv[])
{
QApplication a(argc, argv);

QSplashScreen *splash=new QSplashScreen;
splash->setPixmap(QPixmap(":/snakeimg.jpg"));
splash->show();

MainWindow w;

QTimer::singleShot(1000,splash,SLOT(close()));
QTimer::singleShot(1000,&w,SLOT(show()));

//w.show();


return a.exec();
}
