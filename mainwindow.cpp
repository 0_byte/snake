#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "addtorecordtable.h"
#include "bestscores.h"
#include "gamecontinue.h"
#include <QMessageBox>
#include <QFile>
#include <QDate>
#include <QTextStream>
#include <QApplication>
#include <QInputDialog>
#include <QTimer>
#include "userchooser.h"
#include "player.h"
#include "diffchooser.h"
#include <QDebug>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QDir>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->frame->setFrameStyle(QFrame::StyledPanel);
    ui->frame->setStyleSheet("background-image: url(:/fon25mini.jpg)");
    ui->pbMain->setFocus();
    setFixedSize(this->size());    

    isNewGame = true;

    initPlayer();

    connect(ui->frame, SIGNAL(OnGameOver()), this, SLOT(on_game_over()));
    connect(ui->frame, SIGNAL(OnChangeScores(int)), SLOT(refresh_scores(int)));
    connect(ui->frame, SIGNAL(onChangeSpeedRate(int)), SLOT(refresh_speed(int)));
    connect(ui->push_continue, SIGNAL(released()), this, SLOT(on_push_continue()));
    connect(ui->pbMain, SIGNAL(released()), this, SLOT (gameStart()));
    connect(ui->frame, SIGNAL(sendPlaySound(int)), this, SLOT(playSound(int)));
    connect(ui->soundButt, SIGNAL(released()), this, SLOT(soundMuting()));

    connect(ui->frame, SIGNAL(sendSuperTime(int)),
            ui->superProg, SLOT(setValue(int)));

    connect(ui->frame, SIGNAL(sendStartSuperTimer()),
            this, SLOT(supperAppleVis()));
    connect(ui->frame, SIGNAL(sendStopSuperTimer()),
            this, SLOT(supperAppleUnvis()));
    connect(ui->frame, SIGNAL(sendStartRottenTimer()),
            this, SLOT(rottenAppleVis()));
    connect(ui->frame, SIGNAL(sendStopRottenTimer()),
            this, SLOT(rottenAppleUnvis()));

    connect(ui->frame, SIGNAL(sendIsSuperAppleActive(bool)),
            ui->superCheck, SLOT(setChecked(bool)));

    emit supperAppleUnvis();
    emit rottenAppleUnvis();
}

MainWindow::~MainWindow()
{
    delete ui;
}

//Клік по кнопкі "Розпочати/завершити гру"
void MainWindow::on_push_new_game()
{
    if (ui->frame->IsGameRunning()){
        ui->frame->StopGame();
        setEnabledUI("Розпочати гру", true);
    }else{
        ui->frame->setFocus();
        ui->frame->StartGame(player.getDifficulty());
        emit playSound(4);
        setEnabledUI("Завершити гру", false);
    }
}

//Слот, який оброблює сигнал закінчення гри
void MainWindow::on_game_over()
{
    emit playSound(1);
    player.setSnake_size(ui->frame->snake.size());

    if (ui->frame->getScores() > 0)
    {
    AddToRecordTable dlg(this);
    dlg.SetScores(player.getScore());

    if (dlg.exec()== QDialog::Accepted && dlg.getUserName().length()>0 )
    {
       player.setName(dlg.getUserName());
       player.setLast_game(QDate::currentDate().toString("dd.MM.yyyy"));

      //Записуємо в файл результат героя
      QFile f;
      f.setFileName(QCoreApplication::applicationDirPath().append("/scores.txt"));
      if (f.open(QIODevice::Append| QFile::Text))
      {
         QTextStream s(&f);
         s.setCodec("UTF-8");
         s << player.getName()<< ";"
           << player.getScore() << ";"
           << player.getDifficulty() << ";"
           << player.getSnake_size() << ";"
           << player.getSpeed_rate()<< ";"
           << player.getLast_game() << "\n";
         f.close();
      }      
    }

    }
    else
        QMessageBox::warning(0,"Гру завершено", "Ви не набрали жодного очка :(");

    ui->speed_rate->display(0);
    ui->diff->display(0);
    ui->scores->display(0);

    setEnabledUI("Розпочати гру", true);

    player.setName("");
    player.setDifficulty(1);
    player.setLast_game("");
    player.setScore(0);
    player.setSnake_size(0);
    player.setSpeed_rate(0);
}

//Слот, який оброблює сигнал зміни очків
void MainWindow::refresh_scores(int val)
{
    ui->scores->display(val);
    player.setScore(val);
}

void MainWindow::refresh_speed(int val)
{
   ui->speed_rate->display(val);
   player.setSpeed_rate(val);
}

void MainWindow::on_pushButton_clicked()
{
    emit playSound(3);
    BestScores *dlg = new BestScores(this);
    dlg->exec();
    delete dlg;
}

void MainWindow::on_pushButton_2_clicked()
{
QMessageBox::information(0, "Про автора", "<H2> </H2>"
                                          "\n<H2> </H2>"
                                          "\n<H2> </H2>"
                                          "\n<H2> </H2>");
}

void MainWindow::on_pushButton_3_clicked()
{
QMessageBox::information(0, "Про гру", "<H2>Гравець керує змійкою, яка повзає по площині,збираючи їжу (або інші предмети), уникаючи зіткнення з власним хвостом.</H2>"
                                       "\n<H2>Кожен раз, коли змія з'їдає шматок їжі, вона стає довшою, що поступово ускладнює гру. </H2>"
                                       "\n<H2>Гравець керує напрямом руху голови змії (4 напрямки: вгору, вниз, вліво, вправо), а хвіст змії рухається слідом. Гравець не може зупинити рух змії.</H2>");
}


void MainWindow::gameStart(){
    if(isNewGame){
       //init diff chooser and start new game
        if (!ui->frame->IsGameRunning()){
            DiffChooser *dc = new DiffChooser(this);
            int answer = dc->exec();
            if( answer == QDialog::Accepted){
                player.setDifficulty(dc->getDiff());
                ui->diff->display(player.getDifficulty());              
            }

            if(answer == QDialog::Rejected){
                dc->close();
                return;
            }
        }else{
            ui->speed_rate->display(0);
            ui->diff->display(0);
            ui->scores->display(0);
        }
         emit on_push_new_game();
    }else{
        if (!ui->frame->IsGameRunning()){
            ui->frame->ContinueGame(player.getScore(),
                                    player.getSpeed_rate(),
                                    player.getDifficulty(),
                                    player.getSnake_size());

            setEnabledUI("Завершити гру", false);
            ui->frame->setFocus();
            ui->diff->display(player.getDifficulty());
            emit playSound(4);
        }else{
            ui->frame->StopGame();
            setEnabledUI("Розпочати гру", true);
        }
        isNewGame = true;
    }

}

void MainWindow::playSound(int index)
{
    playlist->setCurrentIndex(index);
    aplayer->setMedia(playlist->currentMedia());

    aplayer->play();
}
bool MainWindow::getIsNewGame() const
{
    return isNewGame;
}

void MainWindow::setIsNewGame(bool value)
{
    isNewGame = value;
}


void MainWindow::on_push_continue(){
    UserChooser *u_ch = new UserChooser(this);
    int call = u_ch->exec();

    if(u_ch->getIsBadData()){
        u_ch->close();
    }
    if(call == QDialog::Accepted ){
            if(u_ch->getIsChoosed()){
                player = u_ch->getP();
                emit gameContinue();
                delete u_ch;
            }
    }
}

void MainWindow::gameContinue(){
    QMessageBox::StandardButton reply = QMessageBox::information(0, "Приготуйся до гри!",
                             "<b>" + player.getName() +"</b>,  Приготуйся"
                             "<br>Очок: " + QString::number(player.getScore()) +
                             "<br>Складнiсть: " + QString::number(player.getDifficulty()) +
                             "<br>Довжина змii: " + QString::number(player.getSnake_size()) +
                             "<br>Швидкiсть: " + QString::number(player.getSpeed_rate()) +
                             "<br>Остання гра: " + player.getLast_game());
    isNewGame = false;

    if(reply == QMessageBox::Ok){
        emit gameStart();
    }
}

Ui::MainWindow *MainWindow::getUi() const
{
    return ui;
}


void MainWindow::paintEvent(QPaintEvent *)

{
    QImage img(":/fon24.jpg");
    QPainter painter(this); // визначає об'єкт painter, який забезпечує малювання
    painter.drawImage(0,0, img.scaled(this->size())); //
}

void MainWindow::setEnabledUI(QString mainButtText, bool enabled){
    ui->pbMain->setText(mainButtText);
    ui->scores->setEnabled(enabled);
    ui->push_continue->setEnabled(enabled);
    ui->pushButton->setEnabled(enabled);
    ui->pushButton_2->setEnabled(enabled);
    ui->pushButton_3->setEnabled(enabled);
}

void MainWindow::initPlayer()
{
    aplayer = new QMediaPlayer();
    playlist = new QMediaPlaylist(aplayer);

    if(!QDir(QCoreApplication::applicationDirPath() + "/sounds").exists()){
        QDir().mkdir(QCoreApplication::applicationDirPath() + "/sounds");
    }
    QFile::copy(":/sounds/sounds/change.mp3" ,
                QCoreApplication::applicationDirPath() + "/sounds/change.mp3");
    QFile::copy(":/sounds/sounds/fail.mp3" ,
                QCoreApplication::applicationDirPath() + "/sounds/fail.mp3");
    QFile::copy(":/sounds/sounds/spacy.mp3" ,
                QCoreApplication::applicationDirPath() + "/sounds/spacy.mp3");
    QFile::copy(":/sounds/sounds/success.mp3" ,
                QCoreApplication::applicationDirPath() + "/sounds/success.mp3");
    QFile::copy(":/sounds/sounds/win.mp3" ,
                QCoreApplication::applicationDirPath() + "/sounds/win.mp3");

    playlist->addMedia(QUrl::fromLocalFile(QCoreApplication::applicationDirPath()
                                           + "/sounds/change.mp3"));
    playlist->addMedia(QUrl::fromLocalFile(QCoreApplication::applicationDirPath()
                                           + "/sounds/fail.mp3"));
    playlist->addMedia(QUrl::fromLocalFile(QCoreApplication::applicationDirPath()
                                           + "/sounds/spacy.mp3"));
    playlist->addMedia(QUrl::fromLocalFile(QCoreApplication::applicationDirPath()
                                           + "/sounds/success.mp3"));
    playlist->addMedia(QUrl::fromLocalFile(QCoreApplication::applicationDirPath()
                                           + "/sounds/win.mp3"));
    aplayer->setVolume(100);
}

void MainWindow::soundMuting()
{
    if(aplayer->volume() == 100){
        aplayer->setVolume(0);
        ui->soundButt->setIcon( * new QIcon(":/mute.png"));
    }else if(aplayer->volume() == 0){
        aplayer->setVolume(100);
        ui->soundButt->setIcon( * new QIcon(":/sound.png"));
    }
}

void MainWindow::supperAppleUnvis()
{
    ui->supperAppleIcon->setEnabled(false);
}

void MainWindow::supperAppleVis()
{
    ui->supperAppleIcon->setEnabled(true);
}

void MainWindow::rottenAppleUnvis()
{
    ui->rottenApple->setEnabled(false);
}

void MainWindow::rottenAppleVis()
{
    ui->rottenApple->setEnabled(true);
}

Player MainWindow::getPlayer()
{
    return player;
}

void MainWindow::setPlayer(Player &value)
{
    player = value;
}





