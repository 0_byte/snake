#include "bestscores.h"
#include "ui_bestscores.h"

#include <QFile>

BestScores::BestScores(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BestScores)
{
    ui->setupUi(this);
    loadFromFile();   
}

BestScores::~BestScores()
{
    delete ui;
}

void BestScores::loadFromFile()
{
  QFile f;
    //сортування робиться таблицею
  f.setFileName(QCoreApplication::applicationDirPath().append("/scores.txt"));

  if (f.exists())
  {
     f.open(QFile::ReadOnly);
     QStringList rows;
     QStringList cols;
     QString data;
     data.clear();
     rows.clear();
     cols.clear();
     //Зчитуємо весь файл в рядок
     data = f.readAll();
     f.close();
     //разбиваємо рядок на масив рядків по символам перекладу рядка
     rows = data.split("\n");
     ui->tblMain->setColumnWidth(0, 250);
     ui->tblMain->setRowCount(0);
     //Встановлюємо кількість рядків в таблиці результатів
     ui->tblMain->setRowCount(rows.count()-1);
     //Записуємо дані в табличку с результатами
     for (int i=0; i<rows.count(); ++i)
     {
         //Skip line if starts with // or empty
         if(rows[i].startsWith("//") || rows[i].isEmpty()){
             continue;
           }
         //Розбиваємо кожний рядок файла на складові частини по символу ;
         cols = rows[i].split(";");
         if (cols.count() > 5)
         {
             QTableWidgetItem *user = new QTableWidgetItem;
             user->setData(Qt::DisplayRole, cols[0]);
             //Робить колонку тільки для читання
             user->setFlags(user->flags() & (~Qt::ItemIsEditable));
             ui->tblMain->setItem(i,0,user);

             QTableWidgetItem *score = new QTableWidgetItem;
             //Перетворимо рядок у число для коректного сортування
             score->setData(Qt::DisplayRole, cols[1].toInt());
             //Робить колонку тільки для читання
             score->setFlags(score->flags() & (~Qt::ItemIsEditable));
             ui->tblMain->setItem(i,1,score);

             QTableWidgetItem *diff = new QTableWidgetItem;
             diff->setData(Qt::DisplayRole, cols[2].toInt());
             diff->setFlags(diff->flags() & (~Qt::ItemIsEditable));
             ui->tblMain->setItem(i,2,diff);

             QTableWidgetItem *snake = new QTableWidgetItem;
             snake->setData(Qt::DisplayRole, cols[3].toInt());
             snake->setFlags(snake->flags() & (~Qt::ItemIsEditable));
             ui->tblMain->setItem(i,3,snake);

             QTableWidgetItem *speed = new QTableWidgetItem;
             speed->setData(Qt::DisplayRole, cols[4].toInt());
             speed->setFlags(speed->flags() & (~Qt::ItemIsEditable));
             ui->tblMain->setItem(i,4,speed);

             QTableWidgetItem *date = new QTableWidgetItem;
             date->setData(Qt::DisplayRole, cols[5]);
             //Робить колонку тільки для читання
             date->setFlags(date->flags() & (~Qt::ItemIsEditable));
             ui->tblMain->setItem(i,5,date);
         }

     }
  }

  //Сортуємо по колонкі з результатами
  ui->tblMain->sortByColumn(1, Qt::DescendingOrder);
}

